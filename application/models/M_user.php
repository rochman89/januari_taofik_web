<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model {

	public function show($limit, $offset){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->order_by('id', 'desc'); 
		$this->db->limit($limit, $offset);
		
		$query = $this->db->get();
		return $query;
	}

	public function add(){
		$data			= $this->upload->data();
		$fname		= $data['file_name'];
		$fpath		= $data['file_path'];
		$nama 		= $this->input->post('nama');
		$email 		= $this->input->post('email');
		$pass 		= md5($this->input->post('password'));
		$jk 			= $this->input->post('jk');
		$no_tlp 	= $this->input->post('no_tlp');
		$kerja		= $this->input->post('pekerjaan');
		$data 		= array(
									'nama' 			=> $nama,
									'email'			=> $email,
									'password'	=> $pass,
									'jk' 				=> $jk,
									'no_tlp' 		=> $no_tlp,
									'pekerjaan'	=> $kerja,
									'fname' 		=> $fname,
									'fpath'			=> $fpath
								);
		$this->db->insert('user', $data);
	}

	public function foto($id) {
		$this->db->select('fname,fpath');
		$this->db->from('user');
		$this->db->where('id', $id);	
		
		$query = $this->db->get();
		return $query->row();
	}
	public function getUser($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id', $id); 
		
		$query = $this->db->get();
		return $query;
	}
	public function hapusUser($id){
		$this->db->where('id', $id);
		$this->db->delete('user');
	}
}
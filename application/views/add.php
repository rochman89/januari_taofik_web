<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-12" style="border: 1px solid #333;">
				<h2><a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button"><--Kembali</a> Tambah User</h2>
				<hr>
				<?php echo form_open_multipart('user/add_user');?>

					<div class='form-group'>
						<label>Nama</label>
						<input type="text" name="nama" class="form-control" value="<?php echo set_value('nama');?>">
						<span style="color: red;"><?php echo form_error('nama'); ?></span>
					</div>
					<div class="row">
						<div class="col">
							<div class='form-group'>
								<label>Password</label>
								<input type="password" name="password" class="form-control">
							</div>
						</div>
						<div class="col">
							<div class='form-group'>
								<label>Konformasi Password</label>
								<input type="password" name="passconf" class="form-control">
								<span style="color: red;"><?php echo form_error('password'); ?></span>
							</div>
						</div>
					</div>
					<div class='form-group'>
						<label>Nomor Telepon / HP</label>
						<input type="text" name="no_tlp"  class="form-control" maxlength="13" value="<?php echo set_value('no_tlp');?>" onkeypress="return hanyaAngka(event)"/>
						<span style="color: red;"><?php echo form_error('no_tlp'); ?></span>
					</div>
					<div class='form-group'>
						<label>E-Mail</label> <!-- HARUS ADA @ NYA-->
						<input type="text" name="email" class="form-control" value="<?php echo set_value('email');?>">
						<span style="color: red;"><?php echo form_error('email'); ?></span>
					</div>
					<div class='form-group'>
						<label>Pekerjaan</label>
						<select class='form-control' name="pekerjaan">
							<option value='Karyawan Swasta'>Karyawan Swasta</option>
							<option value='Pegawai Negeri'>Pegawai Negeri</option>
							<option value='Belum Bekerja'>Belum Bekerja</option>
						</select>
						<span style="color: red;"><?php echo form_error('pekerjaan'); ?></span>
					</div>
					<div class='form-group'>
						<label>Jenis Kelamin</label><br></ber>
						<div class="form-check">
				      <label class="form-check-label" for="radio1">
				        <input type="radio" class="form-check-input" id="radio1" name="jk" value="Perempuan" checked>Perempuan
				      </label>
				    </div>
				    <div class="form-check">
				      <label class="form-check-label" for="radio2">
				        <input type="radio" class="form-check-input" id="radio2" name="jk" value="Laki-Laki">Laki-Laki
				      </label>
				    </div>
						<span style="color: red;"><?php echo form_error('jk'); ?></span>
					</div>
					<div class='form-group'>
						<label>Foto</label>
						<input type="file" class="form-control" name="userfile">
						<span style="color: red;"><?php echo form_error('userfile'); ?></span>
					</div>
					<center>
						<input type="submit" class="btn btn-danger">
					</center>
				</form>	
			</div>
		</div>
	</div>


	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<script>
		function hanyaAngka(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
		    return false;
		  return true;
		}
	</script>
</body>
</html>
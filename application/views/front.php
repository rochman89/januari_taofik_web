<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Project</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">	
	<style>
		.popup{
      margin: auto;
      text-align: center
    }
    .popup img{
        width: 200px;
        height: 200px;
        cursor: pointer
    }
    .show{
        z-index: 999;
        display: none;
    }
    .show .overlay{
        width: 100%;
        height: 100%;
        background: rgba(0,0,0,.66);
        position: absolute;
        top: 0;
        left: 0;
    }
    .show .img-show{
        width: 600px;
        height: 400px;
        background: #FFF;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%,-50%);
        overflow: hidden
    }
    .img-show span{
        position: absolute;
        top: 10px;
        right: 10px;
        z-index: 99;
        cursor: pointer;
    }
    .img-show img{
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
    }
    .news-item { display:inline-block; vertical-align:top; width:300px;} 
    /*End style*/
	</style>
	
</head>
<body>

	<div class="container">
    <div class="row">
  		<h2>Daftar User</h2>
      <?php echo $this->session->flashdata('message');?>
      <table class="table table-bordered table-striped">
        <thead>
        	<tr>
        		<th colspan="8">
        			User
        			<a class="btn btn-primary" href="<?php echo base_url(); ?>user/add_user" role="button" style="float: right;"><b>+</b> Add User</a>
        		</th>
        	</tr>
          <tr>
          	<th>Nama</th>
            <th>Email</th>
            <th>Password</th>
          	<th>No. Tlp/Hp</th>
          	<th>Pekerjaan</th>
          	<th>Foto</th>
          	<th> </th>
          </tr>
        </thead>
        <tbody>
        	<?php 
        		foreach ($user->result() as $value) {
        	?>
          <tr style="font-size:14px;">
          	<th><a href="<?php echo base_url().'user/detail_user/'.$value->id;?>"><?php echo $value->nama; ?></a></th>
          	<th><?php echo $value->email; ?></th>
            <th><?php echo $value->password; ?></th>
            <th><?php echo $value->no_tlp; ?></th>
          	<th><?php echo $value->pekerjaan; ?></th>
          	<th>
          		<div class="popup">
  							<img src="<?php echo base_url().'foto/'.$value->fname; ?>" style="width:80px; height:100px;">
  						</div>
  					</th>
            <td>
            	<!--<a class="btn btn-danger" href="<?php echo base_url().'user/hapus_user/'.$value->id; ?>" role="button"></a>-->
              <a class="btn btn-danger" href="<?php echo base_url().'user/hapus_user/'.$value->id; ?>" onclick="return confirm('Anda yakin akan menghapus user Taofik Rochman ?')" role="button">Delete</a> 
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
		
  		<div class="show">
  		  <div class="overlay"></div>
  		  <div class="img-show">
  		    <span>X</span>
  		    <img src="">
  		  </div>
  		</div>
    </div>
	</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
<script>
	$(function () {
    "use strict";
    
    $(".popup img").click(function () {
        var $src = $(this).attr("src");
        $(".show").fadeIn();
        $(".img-show img").attr("src", $src);
    });
    
    $("span, .overlay").click(function () {
        $(".show").fadeOut();
    });
    
});
</script>
</body>
</html>
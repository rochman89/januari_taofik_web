<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Project</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">	
</head>
<body>

<div class="container">
	<?php foreach ($user->result() as $value) {?>
		<h2><a class="btn btn-primary" href="<?php echo base_url(); ?>" role="button"><--Back</a> <?php echo $value->nama;?></h2>
		<hr>
    <div class="row">
    	<div class="col-4">
    		<img src="<?php echo base_url().'foto/'.$value->fname; ?>" style="width:200px; height:300px;">
    	</div>
    	<div class="col-8">
    		<div>Nama : <?php echo $value->nama;?></div><hr>
    		<div>E-Mail : <?php echo $value->email;?></div><hr>
    		<div>Password : <?php echo $value->password;?></div><hr>
    		<div>Nomor Telepon / HP: <?php echo $value->no_tlp;?></div><hr>
    		<div>Jenis Kelamin : <?php echo $value->jk;?></div><hr>
    		<div>Pekerjaan : <?php echo $value->pekerjaan;?></div><hr>
    	</div>
    </div>
	<?php } ?>
</body>
</html>
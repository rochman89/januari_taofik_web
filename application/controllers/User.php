<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('m_user');
	}
	public function index(){
		redirect('');
	}
	public function add_user(){
		$this->form_validation->set_rules('nama', 'Nama User', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]');
		$this->form_validation->set_rules('passconf', 'Password Konfirmasi', 'trim|required');
		$this->form_validation->set_rules('no_tlp', 'Nomor Telp', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'required');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required');
		if($this->form_validation->run() == FALSE){
			$this->load->view('add');
		}else{
			$config['upload_path']    = './foto/';
			$config['allowed_types']	= 'jpg|png';
			$config['max_size']       = 150000;
			$config['encrypt_name']		= TRUE;
			$this->load->library('upload', $config);
			if ($this->upload->do_upload('userfile')){
				$this->m_user->add();
				$this->session->set_flashdata('message', '<div class="alert alert-info"><button class="close" data-dismiss="alert">&times;</button><strong>Sukses!</strong> User berhasil ditambah.</div>');
				redirect('');
			}else{
				print_r($this->upload->display_errors());
			}
		}
	}

	public function detail_user($id){
		$data['user'] = $this->m_user->getUser($id);
		$this->load->view('detail', $data);
	}

	public function hapus_user($id){
		$data['get'] = $this->m_user->foto($id);
		@unlink($data['get']->fpath. $data['get']->fname);
		$this->m_user->hapusUser($id);
		$this->session->set_flashdata('message', '<div class="alert alert-info"><button class="close" data-dismiss="alert">&times;</button><strong>Sukses!</strong> User berhasil dihapus.</div>');
		redirect('');
	}
}

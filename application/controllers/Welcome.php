<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index(){
		$this->load->model('m_user');
		$data['user'] = $this->m_user->show(10,0);
		$this->load->view('front', $data);
	}
}

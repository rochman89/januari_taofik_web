<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_user extends CI_Migration{


	public function up(){
		$this->dbforge->add_field(array(
			'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            	),
      'nama' => array(
				          'type' => 'VARCHAR',
				          'constraint' => '100',
			      		),
      'email' => array(
				          'type' => 'VARCHAR',
				          'constraint' => '100',
			      		),
      'password' => array(
				          'type' => 'VARCHAR',
				          'constraint' => '100',
			      		),
      'jk' => array(
				          'type' => 'ENUM("Perempuan","Laki-Laki")',
				          'null' => FALSE,
			      		),
      'no_tlp' => array(
				          'type' => 'VARCHAR',
				          'constraint' => '100',
			      		),
      'pekerjaan' => array(
				          'type' => 'ENUM("Karyawan Swasta","Pegawai Negeri","Belum Bekerja")',
				          'null' => FALSE,
			      		),
      'fname' => array(
				          'type' => 'VARCHAR',
				          'constraint' => '100',
			      		),
      'fpath' => array(
				          'type' => 'VARCHAR',
				          'constraint' => '100',
			      		),
		));
		$this->dbforge->add_key('id');
   	$this->dbforge->create_table('user');
	}

	public function down() {
		$this->dbforge->drop_table('user');
	}
}